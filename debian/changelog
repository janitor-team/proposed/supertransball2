supertransball2 (1.5-9) unstable; urgency=medium

  * Switch to compat level 11.
  * Drop deprecated menu file.
  * Declare compliance with Debian Policy 4.2.0.
  * Fix FTCBFS: Let dh_auto_build pass cross tools to make.
    Thanks to Helmut Grohne for the report and patch. (Closes: #902537)

 -- Markus Koschany <apo@debian.org>  Mon, 13 Aug 2018 10:37:11 +0200

supertransball2 (1.5-8) unstable; urgency=medium

  * Declare compliance with Debian Policy 3.9.8.
  * Vcs-Git: Use https.
  * Add gcc-6-convert-bool.patch and fix FTBFS with GCC-6. (Closes: #811617)
    Thanks to Martin Michlmayr for the report.
  * Update hardening.patch and use $CXX instead of c++.
  * Ensure that supertransball2 can be built twice in a row. Update
    debian/clean.

 -- Markus Koschany <apo@debian.org>  Fri, 08 Jul 2016 00:21:14 +0200

supertransball2 (1.5-7) unstable; urgency=medium

  * Declare compliance with Debian Policy 3.9.6.
  * Vcs-Browser: Switch to cgit and https.
  * d/control: Remove hardening-wrapper because it is going to be removed from
    Debian. Add hardening.patch instead and use dpkg-buildflags.
  * Add keywords to desktop file.
  * Resize png icon to 128x128 pixel and install it to hicolor directory.
  * Drop source/local-options file.
  * Add clean file and ensure that supertransball2 can be built twice in a row.

 -- Markus Koschany <apo@debian.org>  Wed, 28 Oct 2015 21:39:10 +0100

supertransball2 (1.5-6) unstable; urgency=low

  * debian/rules:
    - Use separate dh_auto_install-arch and dh_auto_install-indep
      overrides to avoid a FTBFS on the build servers.

 -- Markus Koschany <apo@gambaru.de>  Wed, 16 Jan 2013 17:02:03 +0100

supertransball2 (1.5-5) unstable; urgency=low

  * New Maintainer. (Closes: #661457)
  * Switch to package format 3.0 (quilt). Thanks Jari Aalto for the patch!
    (Closes: #668030).
  * Remove patch for command line option -f for fullscreen, it does not work.
    Instead improve supertransball2's manpage and suggest using the ingame key
    combinations ALT+1,2,3,4 or ALT+ENTER to control the video mode.
    (Closes: #590636)
  * Bump compat level to 9 and require debhelper >=9 for automatic hardening
    build flags.
  * Merge the old patches into one and modify the Makefile. Don't link against
    libsdl_sound anymore to avoid a superfluous dependency.
  * debian/control:
  - Add hardening wrapper to Build Depends.
  - Add Vcs-fields and point to supertransball2's git repository at
    git.debian.org.
  - Improve the package description.
  * debian/rules:
  - Use dh sequencer to simplify debian/rules.
  - Split readme.txt in README and changelog and install them in the
    right place.
  - Build with hardening=+all.
  * Add a desktop file, menu file and icons.
  * Update debian/copyright to copyright format 1.0.

 -- Markus Koschany <apo@gambaru.de>  Mon, 29 Oct 2012 15:48:17 +0100

supertransball2 (1.5-4) unstable; urgency=low

  * QA upload as part of Salzburg BSP.
  * Set Maintainer field to QA Group as the package has been orphaned.
    closes: 675219

 -- Willi Mann <willi@debian.org>  Fri, 15 Jun 2012 15:10:04 +0200

supertransball2 (1.5-3) unstable; urgency=low

  * Patch to fix config file recreation. (closes: #334841)
  * Patch to fix crash cause of checking. (closes: #335166)
  * Update manual page.
  * Update standards version.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun,  6 Nov 2005 18:28:30 +0100

supertransball2 (1.5-2) unstable; urgency=low

  * Allocate enough memory for C string. (closes: #311861)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Tue,  7 Jun 2005 13:18:32 +0200

supertransball2 (1.5-1) unstable; urgency=low

  * Initial Release. (closes: #260882)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Tue, 20 Jul 2004 00:47:46 +0200

